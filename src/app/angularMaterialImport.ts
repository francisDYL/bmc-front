import { MatSidenavModule } from '@angular/material';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatDialogModule} from '@angular/material/dialog';
import {TextFieldModule} from '@angular/cdk/text-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
export const AngularMaterialModules = [
	MatSidenavModule,
	DragDropModule,
	MatDialogModule,
	TextFieldModule,
	MatInputModule,
	MatButtonModule,
	MatIconModule,
	MatMenuModule,
	MatSelectModule,
	MatToolbarModule,
	MatListModule,
	MatTableModule,
	MatExpansionModule,
	MatCardModule

];
