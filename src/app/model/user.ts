
export interface User {
	uid?: string;
   	displayName?: string;
   	photoURL?: string;
   	emailVerified?: boolean;
	email?: string;
	phoneNumber?: any;
	isAnonymous?: boolean;
	role?: string;
}
