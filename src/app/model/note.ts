export interface Note {
	uid?: string;
	title: string;
	content?: string;
	color?: string;
	category?: string;
	oldCategory?: string;
	project?: string;
}

