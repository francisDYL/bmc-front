export const environment = {
  production: true,
	storageKey: 'sessionStorage',
	userKey: 'bmconlineUser',
	firebaseConfig : {
		apiKey: "AIzaSyBRfy0F3ozhhq6cI5WaNHtkZqvtCTx-h6s",
		authDomain: "bmconline-5cee2.firebaseapp.com",
		databaseURL: "https://bmconline-5cee2.firebaseio.com",
		projectId: "bmconline-5cee2",
		storageBucket: "bmconline-5cee2.appspot.com",
		messagingSenderId: "920704956145",
		appId: "1:920704956145:web:6877b55e973db3f577db50",
		measurementId: "G-VXHRSCZQM2"
	}
};
